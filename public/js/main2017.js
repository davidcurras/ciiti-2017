$( document ).ready(function() {
    var games2017 = [
      {
        name: 'game/CIITI2017/carrera-ciiti',
        image: 'img/2017/carrera.png'
      },
      {
        name: 'game/CIITI2017/insanity-infiltration',
        image: 'img/2017/infiltration.png'
      },
      {
        name: 'game/CIITI2017/servicio-a-tiempo',
        image: 'img/2017/tiempo.png'
      },
      {
        name: 'game/CIITI2017/hora-de-correr',
        image: 'img/2017/alarma.png'
      }
    ];
    var games2017Shufled = _.shuffle(games2017);
    var html = '';
    var arr;
    for (var i = 0; i < games2017Shufled.length; i++) {
      arr = [
        '<div>',
        '<a href="'+games2017Shufled[i].name+'">',
        '<img src="'+games2017Shufled[i].image+'" alt>',
        '</div>'
      ];
      html += arr.join('\n');
    }
    $( '#gallery2017' ).append( html );
});
