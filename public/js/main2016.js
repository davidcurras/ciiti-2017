$( document ).ready(function() {
    var games2016 = [
      {
        name: 'game/CIITI2016/CiroAngeleri/deflect/public',
        image: 'img/2016/deflect.png'
      },
      {
        name: 'game/CIITI2016/DavidCurras/veggie-zombie/public',
        image: 'img/2016/zombie1.png'
      },
      {
        name: 'game/CIITI2016/EstebanFrare/hallucination/public',
        image: 'img/2016/halluc.png'
      },
      {
        name: 'game/CIITI2016/GonzoGhanem/chaos/public',
        image: 'img/2016/chaos.png'
      },
      {
        name: 'game/CIITI2016/JuaneNieva/all-eyes-on-you/public',
        image: 'img/2016/alleyes.png'
      },
      {
        name: 'game/CIITI2016/JulianCedaro/head-soccer/public',
        image: 'img/2016/headsoccer.png'
      },
      {
        name: 'game/CIITI2016/Lauro/black-and-white/public',
        image: 'img/2016/black.png'
      },
      {
        name: 'game/CIITI2016/Lauro/me-quiere-no-me-quiere/public',
        image: 'img/2016/quiere.png'
      },
      {
        name: 'game/CIITI2016/Lauro/galaxy-jump/public',
        image: 'img/2016/galaxy.png'
      },
      {
        name: 'game/CIITI2016/SebastianSilva/the-scape/public',
        image: 'img/2016/thescape.png'
      },
      {
        name: 'game/CIITI2016/SebastianFlores/icecream-zombies/public',
        image: 'img/2016/icecream.png'
      },
      {
        name: 'game/CIITI2016/JesusQuiroga/eagle-eye/public',
        image: 'img/2016/eagle.png'
      }
    ];
    var games2016Shufled = _.shuffle(games2016);
    var html = '';
    var arr;
    for (var i = 0; i < games2016Shufled.length; i++) {
      arr = [
        '<div>',
        '<a href="'+games2016Shufled[i].name+'">',
        '<img src="'+games2016Shufled[i].image+'" alt>',
        '</div>'
      ];
      html += arr.join('\n');
    }
    $( '#gallery2016' ).append( html );
});
